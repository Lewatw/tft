import tcod
from loguru import logger
from new_game.initialise_new_game import get_constants, new_game_variables, build_pcg, get_next_random_seed
from main_screens.mainMenuScreen import main_menu_screen
from input_handlers import handle_main_menu
from processors.render import render_hud, render

from map_stuff.game_map import GameMap
from map_stuff.fov_functions import initialize_fov, recompute_fov


from utilities.takeScreenshot import Screenshot


def play_game(con, constants, dungeons_master_seeds):

    playing_the_game = True
    fov_recompute = True
    all_dungeons_seeds = []

    for seed in range(constants['number_dungeons']):
        all_dungeons_seeds.append(get_next_random_seed(dungeons_master_seeds))

    # demo code
    for seed in range(constants['number_dungeons']):
        logger.info('Dungeon {} master seed is {ds}', seed, ds=all_dungeons_seeds[seed])

    # build game map
    game_map = GameMap(constants['map_width'], constants['map_height'])
    game_map.make_map(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
                      constants['map_width'], constants['map_height'])

    fov_map = initialize_fov(game_map)

    key = tcod.Key()
    mouse = tcod.Mouse()

    while playing_the_game:
        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS | tcod.EVENT_MOUSE, key, mouse)
        if fov_recompute:
            recompute_fov(fov_map, constants['fov_radius'],
                          constants['fov_light_walls'], constants['fov_algorithm'])

            render_hud(con, constants['screen_width'], constants['screen_height'], constants['map_width'], constants['map_height'])

            render(con, constants['screen_width'], constants['screen_height'],fov_recompute, game_map, fov_map,
                   constants['colours'],constants['hud_x_left_column'],constants['hud_y_top_row'])

        tcod.console_flush()

        action = handle_main_menu(key)

        exit_game = action.get('exit')
        screen_shot = action.get('screen_shot')

        if screen_shot:
            Screenshot.grab_screen_shot()

        if exit_game:
            break


def main():

    constants = get_constants()

    logger.add(constants['logfile'], format=constants['logformat'])

    logger.info('********************')
    logger.info('* New game started *')
    logger.info('********************')

    logger.info('Game Constants have been initialised')
    logger.info('Screen dimensions set to {width} x {height} chars.', width=constants['screen_width'], height=constants['screen_height'])
    logger.info('Game map is set to {width} x {height} chars.', width=constants['map_width'], height=constants['map_height'])

    logger.info('World seed {}', constants['world_seed'])

    tcod.console_set_custom_font('static/courier12x12_aa_tc.png', tcod.FONT_TYPE_GREYSCALE | tcod.FONT_LAYOUT_TCOD)

    tcod.console_init_root(constants['screen_width'], constants['screen_height'], constants['game_window_title'], False)

    con = tcod.console_new(constants['screen_width'], constants['screen_height'])

    show_main_menu = True
    key = tcod.Key()
    mouse = tcod.Mouse()
    menu_not_logged = True

    main_menu_background_image = tcod.image_load('static/menu_background.png')

    while not tcod.console_is_window_closed():
        tcod.sys_check_for_event(tcod.EVENT_KEY_PRESS | tcod.EVENT_MOUSE, key, mouse)
        if show_main_menu:
            # show main menu
            if menu_not_logged:
                logger.info('Building main menu screen')
                menu_not_logged = False
            main_menu_screen(con, constants['screen_width'], constants['screen_height'], constants['game_window_title'],main_menu_background_image)

            tcod.console_flush()

            action = handle_main_menu(key)

            new_game = action.get('new_game')
            load_saved_game = action.get('load_game')
            exit_game = action.get('exit')
            screen_shot = action.get('screen_shot')

            if screen_shot:
                Screenshot.grab_screen_shot()

            if new_game:
                new_game_variables(constants)
                logger.info('Choosing character')
                show_main_menu = False
            elif load_saved_game:
                logger.info('Loading saved game')
            elif exit_game:
                logger.info('********************')
                logger.info('*   Game ending    *')
                logger.info('********************')
                break
        else:
            # play the game
            logger.info('Game is starting...')
            master_dungeons_seed = build_pcg(constants['world_seed'], constants['dungeon_stream'])

            tcod.console_clear(con)
            play_game(con, constants, master_dungeons_seed)

            # this will re-show the main menu/start screen
            show_main_menu = True
            menu_not_logged = True


if __name__ == '__main__':
    main()


