import random
import tcod
from utilities.randomNumberGenerator import PCG32Generator

from loguru import logger


def get_constants():

    game_window_title = 'Tales from Ticronem'
    screen_width = 100
    screen_height = 60

    map_width = 80
    map_height = 40

    player_seed = 63988644

    log_folder = 'logs/'
    log_filename = 'gamelog_'
    log_extension = '.log'
    log_time = '{time:DD-MM-YYYY at HH:mm:ss.SSS}'
    logfile = log_folder + log_filename + log_time + log_extension
    logformat = log_time + ' | {level} | {message}'
    dungeon_stream = 0
    number_of_dungeons = 10
    # BSP variables not currently being used
    bsp_depth = 10          # number of recursive rooms
    bsp_min_size = 5        # smallest room
    bsp_full_rooms = False  # FALSE = create random sized rooms, TRUE = rooms will fill the space within the node
    # simple-dungeon room information
    room_max_size = 10
    room_min_size = 6
    max_rooms = 30
    hud_x_left_column = 2
    hud_y_top_row = 2

    if player_seed > 0:
        world_seed = player_seed
    else:
        world_seed = random.getrandbits(30)

    colors = {
        'dark_wall': tcod.dark_yellow,
        'dark_ground': tcod.Color(50, 50, 150),
        'light_wall': tcod.Color(130, 110, 50),
        'light_ground': tcod.Color(200, 180, 50)
    }

    fov_algorithm = 0
    fov_light_walls = True
    fov_radius = 10

    constants = {
        'game_window_title': game_window_title,
        'screen_height': screen_height,
        'screen_width': screen_width,
        'logformat': logformat,
        'logfile': logfile,
        'map_width': map_width,
        'map_height': map_height,
        'world_seed': world_seed,
        'dungeon_stream': dungeon_stream,
        'number_dungeons': number_of_dungeons,
        'bsp_depth':bsp_depth,
        'bsp_min_size': bsp_min_size,
        'bsp_full_rooms': bsp_full_rooms,
        'room_max_size': room_max_size,
        'room_min_size': room_min_size,
        'max_rooms': max_rooms,
        'colours': colors,
        'fov_algorithm': fov_algorithm,
        'fov_light_walls': fov_light_walls,
        'fov_radius': fov_radius,
        'hud_x_left_column': hud_x_left_column,
        'hud_y_top_row': hud_y_top_row

    }

    return constants


def build_pcg(world_seed, stream):
    return PCG32Generator(world_seed, stream)


def get_next_random_seed(seed):
    return seed.get_next_uint32()


def get_next_seed_bound(seed, boundary):
    """
    Return a value between zero (inclusive) and *bound* (exclusive).
    """
    return seed.get_next_uint(boundary)


def new_game_variables(constants):

    logger.info('Defining new game variables')
    pass

    return True
