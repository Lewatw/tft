import tcod as libtcod

from main_screens.menus import menu


def main_menu_screen(con, screen_width, screen_height, game_title, background_image):
    libtcod.image_blit_2x(background_image, 0, 0, 0)
    libtcod.console_set_default_foreground(0, libtcod.light_yellow)
    libtcod.console_print_ex(0, int(screen_width / 2), int(screen_height / 2) - 8, libtcod.BKGND_NONE, libtcod.CENTER,
                             game_title)
    libtcod.console_print_ex(0, int(screen_width / 2), int(screen_height / 2) + 8, libtcod.BKGND_NONE, libtcod.CENTER,
                             '(c) Steven Devonport 2018')
    menu(con, '', ['Play a new game', 'Continue last game', 'Quit'], 24, screen_width, screen_height)

