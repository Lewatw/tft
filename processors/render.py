import tcod
import esper

from enum import Enum, auto


class RenderOrder(Enum):

    STAIRS = auto()
    CORPSE = auto()
    ITEM = auto()
    ACTOR = auto()


def render(con, screen_width, screen_height, fov_recompute, game_map, fov_map, colours, x_col_adj, y_row_adj):
    if fov_recompute:
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = tcod.map_is_in_fov(fov_map, x, y)
                wall = game_map.tiles[x][y].block_sight

                # if visible:
                #     if wall:
                #         tcod.console_set_char_background(con, x, y, colours.get('light_wall'), tcod.BKGND_SET)
                #     else:
                #         tcod.console_set_char_background(con, x, y, colours.get('light_ground'), tcod.BKGND_SET)
                #     game_map.tiles[x][y].explored = True
                # elif game_map.tiles[x][y].explored:
                if wall:
                    tcod.console_set_char_background(con, x_col_adj + x, y_row_adj + y, colours.get('dark_wall'), tcod.BKGND_SET)
                else:
                    tcod.console_set_char_background(con, x_col_adj + x, y_row_adj + y, colours.get('dark_ground'), tcod.BKGND_SET)

    tcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)


def render_hud(con, screen_width, screen_height, map_width, map_height):

    tcod.console_set_default_background(con, tcod.black)

    dd = 1

    # hardcoded HUD
    for down in range(map_height):
        draw_entity(con, '|', 1, dd, tcod.white, tcod.black)
        draw_entity(con, '|', map_width + 2, dd, tcod.white, tcod.black)
        dd += 1
    copdd = dd + 1

    for across in range(map_width + 2):
        draw_entity(con, '-', across + 1, 1, tcod.white, tcod.black)
        draw_entity(con, '-', across + 1, map_height + 1, tcod.white, tcod.black)

    draw_entity(con, '@', 10, 10, tcod.blue, tcod.black )
    dd +=1

    # message log

    log_width = 50
    log_lines = 9

    # draw edges
    for down in range(log_lines):
        draw_entity(con, '*', 1, dd, tcod.white, tcod.black)
        draw_entity(con, '*', log_width, dd, tcod.white, tcod.black)
        my_line = 'Message log line ' + str(down) + ' 50 chars.'
        tcod.console_print(con, 10, dd, my_line)
        dd += 1

    xx = dd

    # draw bars across
    for across in range (log_width):
        draw_entity(con, '*', across + 1, (map_height + 2), tcod.white, tcod.black)
        draw_entity(con, '*', across + 1, dd, tcod.white, tcod.black)

    dd +=1

    # spell bar
    spbar = '+------+'
    spside = '|      |'

    spkey1 = '|1     |'
    spell1 = '|  %c^%c   |'
    spkey2 = '|2     |'
    spell2 = '|  %c!%c   |'
    spkey3 = '|3     |'
    spell3 = '|  %c^%c   |'
    spkey4 = '|4     |'
    spell4 = '|  %c$%c   |'
    spkey5 = '|5     |'
    spell5 = '|  %c\%c   |'
    spkey6 = '|6     |'
    spell6 = '|  %c^%c   |'
    spkey7 = '|7     |'
    spell7 = '|  %c&%c   |'
    spkey8 = '|8     |'
    spell8 = '|  %c*%c   |'
    spkey9 = '|9     |'
    spell9 = '|  %c(%c   |'
    spkey0 = '|0     |'
    spell0 = '|   %c)%c  |'

    tcod.console_print(con, 2, dd, spbar)
    tcod.console_print(con, 10, dd, spbar)
    tcod.console_print(con, 18, dd, spbar)
    tcod.console_print(con, 26, dd, spbar)
    tcod.console_print(con, 34, dd, spbar)
    tcod.console_print(con, 42, dd, spbar)
    tcod.console_print(con, 50, dd, spbar)
    tcod.console_print(con, 58, dd, spbar)
    tcod.console_print(con, 64, dd, spbar)
    tcod.console_print(con, 72, dd, spbar)

    dd +=1
    tcod.console_print(con, 2, dd, spkey1)
    tcod.console_print(con, 10, dd, spkey2)
    tcod.console_print(con, 18, dd, spkey3)
    tcod.console_print(con, 26, dd, spkey4)
    tcod.console_print(con, 34, dd, spkey5)
    tcod.console_print(con, 42, dd, spkey6)
    tcod.console_print(con, 50, dd, spkey7)
    tcod.console_print(con, 58, dd, spkey8)
    tcod.console_print(con, 64, dd, spkey9)
    tcod.console_print(con, 72, dd, spkey0)
    dd +=1
    tcod.console_print(con, 2, dd, spside)
    tcod.console_print(con, 10, dd, spside)
    tcod.console_print(con, 18, dd, spside)
    tcod.console_print(con, 26, dd, spside)
    tcod.console_print(con, 34, dd, spside)
    tcod.console_print(con, 42, dd, spside)
    tcod.console_print(con, 50, dd, spside)
    tcod.console_print(con, 58, dd, spside)
    tcod.console_print(con, 64, dd, spside)
    tcod.console_print(con, 72, dd, spside)
    dd +=1
    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.yellow, tcod.black)
    tcod.console_print(con, 2, dd, spell1 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.yellow, tcod.black)
    tcod.console_print(con, 10, dd, spell2 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.yellow, tcod.black)
    tcod.console_print(con, 18, dd, spell3 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.blue, tcod.black)
    tcod.console_print(con, 26, dd, spell4 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.blue, tcod.black)
    tcod.console_print(con, 34, dd, spell5 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.green, tcod.black)
    tcod.console_print(con, 42, dd, spell6 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.red, tcod.black)
    tcod.console_print(con, 50, dd, spell7 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.red, tcod.black)
    tcod.console_print(con, 58, dd, spell8 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.red, tcod.black)
    tcod.console_print(con, 64, dd, spell9 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))

    tcod.console_set_color_control(tcod.COLCTRL_1, tcod.white, tcod.black)
    tcod.console_print(con, 72, dd, spell0 %(tcod.COLCTRL_1,tcod.COLCTRL_STOP))
    dd +=1
    tcod.console_print(con, 2, dd, spside)
    tcod.console_print(con, 10, dd, spside)
    tcod.console_print(con, 18, dd, spside)
    tcod.console_print(con, 26, dd, spside)
    tcod.console_print(con, 34, dd, spside)
    tcod.console_print(con, 42, dd, spside)
    tcod.console_print(con, 50, dd, spside)
    tcod.console_print(con, 58, dd, spside)
    tcod.console_print(con, 64, dd, spside)
    tcod.console_print(con, 72, dd, spside)
    dd +=1
    tcod.console_print(con, 2, dd, spbar)
    tcod.console_print(con, 10, dd, spbar)
    tcod.console_print(con, 18, dd, spbar)
    tcod.console_print(con, 26, dd, spbar)
    tcod.console_print(con, 34, dd, spbar)
    tcod.console_print(con, 42, dd, spbar)
    tcod.console_print(con, 50, dd, spbar)
    tcod.console_print(con, 58, dd, spbar)
    tcod.console_print(con, 64, dd, spbar)
    tcod.console_print(con, 72, dd, spbar)
    dd +=1

    left_side = log_width + 2
    boon_down = xx - 5
    condition_down = xx - 4
    control_down = xx -3
    health_down = xx
    mana_down = xx - 1
    f1_down = xx -2


    # boons panel
    for x in '0123456789':
        draw_entity(con, x, left_side + int(x), boon_down, tcod.green, tcod.black)          # boons
        draw_entity(con, x, left_side + int(x), condition_down, tcod.red, tcod.black)       # conditions
        draw_entity(con, x, left_side + int(x), control_down, tcod.light_gray, tcod.black)  # controls

    # health/mana/F1 bar
    for across in range(21):
        draw_entity(con, '#', left_side + across, f1_down, tcod.green, tcod.black)   # F1 bar
        draw_entity(con, '#', left_side + across, mana_down, tcod.blue, tcod.black)    # mana bar
        draw_entity(con, '#', left_side + across, health_down, tcod.red, tcod.black)     # health bar

    tcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)


def draw_entity(con, entity, across, down, fore, back):
    tcod.console_set_default_foreground(con, tcod.white)
    tcod.console_put_char_ex(con, across, down, entity, fore, back )